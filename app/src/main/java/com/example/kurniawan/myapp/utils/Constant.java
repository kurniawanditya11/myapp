package com.example.kurniawan.myapp.utils;

/**
 * @author yudirahmat
 */

public class Constant {
    public static final String TAG                              = "BaseProject";
    public static final String APP_DIR_OPEN                     = "/BaseProject";
    public static final String APP_DIR_IMAGES                   = "Data/Images/";

    public static final String BASE_URL                         = "http://nasyidjayacleaning.com/";
    public static final String BASE_API                         = "http://nasyidjayacleaning.com/rekomendasi/";
//    public static final String BASE_API                         = "http://localhost/rekomendasi/";

    //enable debug
    public static final boolean ENABLE_DEBUG                    = false;
    public static final boolean ENABLE_DEVELOP                  = false;

}