package com.example.kurniawan.myapp.utils;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by Yudi Rahmat
 * Email yudirahmat7@gmail.com
 */

public class Utils {
    public static String getResponse(Response<ResponseBody> response) throws IOException {
        String json = null;

        if(response != null) {
            try {
                if (response.code() == 200)
                    json = new String(response.body().bytes());
                else
                    json = new String(response.errorBody().bytes());
            } catch (Exception e) {
                if (response.code() == 200)
                    json = response.body().string();
                else
                    json = response.body().string();
                e.printStackTrace();
            }
        }

        return json;
    }

    public static double convertStringToDouble(String mValue) {
        double value = 0;

        try {
            value   = Double.parseDouble(mValue);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return value;
    }


    public static String replaceChar(String data, String olChar, String newChar) {
        try {
            data = data.replace(olChar, newChar);
        } catch (Exception e) {}

        return data;
    }
}
