package com.example.kurniawan.myapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RekomendasiDataModel {
    @SerializedName("jenis_kelamin")
    @Expose
    private String jeniskelamin;

    @SerializedName("usia")
    @Expose
    private String usia;

    @SerializedName("interest")
    @Expose
    private String interest;

    @SerializedName("hadiah")
    @Expose
    private String hadiah;

    public String getJeniskelamin() {
        return jeniskelamin;
    }

    public void setJeniskelamin(String jeniskelamin) {
        this.jeniskelamin = jeniskelamin;
    }

    public String getUsia() {
        return usia;
    }

    public void setUsia(String usia) {
        this.usia = usia;
    }

    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }

    public String getHadiah() {
        return hadiah;
    }

    public void setHadiah(String hadiah) {
        this.hadiah = hadiah;
    }
}
