package com.example.kurniawan.myapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class UserListModel {
    @SerializedName( "data" )
    @Expose
    private List<UserDataModel> data = new ArrayList<>();

    public List <UserDataModel> getData() {
        return data;
    }

    public void setData(List<UserDataModel> data){
        this.data = data;
    }
}
