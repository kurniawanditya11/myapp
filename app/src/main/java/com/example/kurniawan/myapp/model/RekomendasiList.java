package com.example.kurniawan.myapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class RekomendasiList {
    @SerializedName("data")
    @Expose
    private List<String> data = null;
    @SerializedName("toko")
    @Expose
    private List<TokoDataModel> toko = null;

    public List<String> getData() {
        return data;
    }

    public void setData(List<String> data) {
        this.data = data;
    }

    public List<TokoDataModel> getToko() {
        return toko;
    }

    public void setToko(List<TokoDataModel> toko) {
        this.toko = toko;
    }
}
