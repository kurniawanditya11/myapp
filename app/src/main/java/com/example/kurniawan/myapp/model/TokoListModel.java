package com.example.kurniawan.myapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;



public class TokoListModel {
    @SerializedName("data")
    @Expose
    private List<TokoDataModel> data = new ArrayList<>();

    public List <TokoDataModel> getData() {
        return data;
    }

    public void setData(List<TokoDataModel> data){
        this.data = data;
    }
}
