package com.example.kurniawan.myapp.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.Network;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.kurniawan.myapp.BottomActivity;
import com.example.kurniawan.myapp.MainActivity;
import com.example.kurniawan.myapp.R;
import com.example.kurniawan.myapp.helpers.Rest;
import com.example.kurniawan.myapp.helpers.services.APIService;
import com.example.kurniawan.myapp.utils.NetworkHelper;
import com.example.kurniawan.myapp.utils.Singleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class Login extends AppCompatActivity{
    ProgressDialog pDialog;
    Button btn_register, btn_login;
    EditText txt_username, txt_password;
    Intent intent;

    int success;
    ConnectivityManager conMgr;

    private String url = Server.BASE_API_URL + "login.php";
    private static final String TAG = Login.class.getSimpleName();
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE= "message";

    public final static String TAG_USERNAME = "username";
    public final static String TAG_ID = "id";

    String tag_json_obj = "json_obj_req";

    SharedPreferences sharedPreferences;
    Boolean session = false;
    String id, username;
    public static final String my_shared_preferences = "my_shared";
    public static final String session_status = "session_status";

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_login );

        conMgr = (ConnectivityManager) getSystemService( Context.CONNECTIVITY_SERVICE );{
            if (conMgr.getActiveNetworkInfo() != null
                    && conMgr.getActiveNetworkInfo().isAvailable()
                    && conMgr.getActiveNetworkInfo().isConnected()){
            }else {
                Toast.makeText( getApplicationContext(), "No Internet Connection",
                        Toast.LENGTH_SHORT).show();
            }
        }
        btn_login = (Button) findViewById( R.id.btn_login );
        btn_register = (Button) findViewById( R.id.btn_register );
        txt_username = (EditText) findViewById( R.id.txtusername );
        txt_password = (EditText) findViewById( R.id.txtpassword );

        //cek session jika TRUE
        sharedPreferences = getSharedPreferences( my_shared_preferences, Context.MODE_PRIVATE );
        session = sharedPreferences.getBoolean( session_status, false );
        id = sharedPreferences.getString( TAG_ID, null );
        username = sharedPreferences.getString( TAG_USERNAME, null );

        btn_login.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkLogin(txt_username.getText().toString(), txt_password.getText().toString());
            }
        } );
        if (session){
            Intent intent = new Intent( Login.this, BottomActivity.class );
            intent.putExtra( TAG_ID, id );
            intent.putExtra( TAG_USERNAME, username );
            finish();
            startActivity( intent );
        }

        btn_register.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(Login.this, Register.class);
                finish();
                startActivity(intent);
            }
        } );

    }
    private void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
    }

    private void checkLogin (final String username, final String password) {
        pDialog = new ProgressDialog( this );
        pDialog.setCancelable( false );
        pDialog.setMessage( "Logging In ...." );
        showDialog();


        if (NetworkHelper.isConnectedToInternet(Singleton.getInstance().getApplicationContext())) {
            Rest.make(APIService.class).login(username, password).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                    hideDialog();
                    try {
                        if(response != null && response.isSuccessful()) {
                            showToast( response.toString());
                            showToast(response.code() + "\n\n" + "Berhasil Login");
                            Intent intent = new Intent(Login.this, BottomActivity.class);
                            startActivity(intent);
                        } else {
                            showToast(response.code() + "\n\n" + "Gagal Login");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        showToast(0 + "\n\n" + "Gagal Login");
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    showToast(500 + "\n\n" + "Gagal Login");
                }
            });
        }
    }
        private void showDialog() {
            if (!pDialog.isShowing())
                pDialog.show();
        }

        private void hideDialog() {
            if (pDialog.isShowing())
                pDialog.dismiss();
        }
    }
