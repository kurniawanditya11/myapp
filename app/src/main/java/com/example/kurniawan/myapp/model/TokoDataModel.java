package com.example.kurniawan.myapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class TokoDataModel {
    @SerializedName(value="id_toko", alternate={"id",})
    @Expose
    private String idToko;
    @SerializedName("nm_toko")
    @Expose
    private String nmToko;
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("lon")
    @Expose
    private String lon;
    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("alamat")
    @Expose
    private String alamat;

    public String getIdToko() {
        return idToko;
    }

    public void setIdToko(String idToko) {
        this.idToko = idToko;
    }

    @SerializedName("kat")
    @Expose
    private int kat;

    public String getNmToko() {
        return nmToko;
    }

    public void setNmToko(String nmToko) {
        this.nmToko = nmToko;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public int getKat() {
        return kat;
    }

    public void setKat(int kat) {
        this.kat = kat;
    }
}
