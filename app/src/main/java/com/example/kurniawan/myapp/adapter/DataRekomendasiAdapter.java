package com.example.kurniawan.myapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.kurniawan.myapp.R;
import com.example.kurniawan.myapp.model.RekomendasiDataModel;

import java.util.List;

public class DataRekomendasiAdapter extends RecyclerView.Adapter<DataRekomendasiAdapter.ViewHolder>{
    private List<RekomendasiDataModel> Lrekomendasi;

    public DataRekomendasiAdapter(List<RekomendasiDataModel> Lrekomendasi) {
        this.Lrekomendasi = Lrekomendasi;
    }

    @Override
    public DataRekomendasiAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate( R.layout.list_rekomendasi, parent, false );
        return new DataRekomendasiAdapter.ViewHolder( v );
    }

    @Override
    public void onBindViewHolder(DataRekomendasiAdapter.ViewHolder holder, int position) {
        holder.jenis_kelamin.setText( Lrekomendasi.get(position).getJeniskelamin());
        holder.usia.setText( Lrekomendasi.get(position).getUsia());
        holder.interest.setText( Lrekomendasi.get(position).getInterest());
        holder.hadiah.setText( Lrekomendasi.get(position).getHadiah());
    }

    @Override
    public int getItemCount() {return Lrekomendasi.size(); }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView jenis_kelamin, usia, interest, hadiah;
        public ViewHolder (View view){
            super(view);
            jenis_kelamin= (TextView) view.findViewById( R.id.tvjk );
            usia= (TextView) view.findViewById( R.id.tvumur );
            interest= (TextView) view.findViewById( R.id.tvinterest );
            hadiah= (TextView) view.findViewById( R.id.tvhadiah );
        }
    }
}
