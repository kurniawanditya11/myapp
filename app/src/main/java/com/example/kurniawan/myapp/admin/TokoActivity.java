package com.example.kurniawan.myapp.admin;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.example.kurniawan.myapp.R;
import com.example.kurniawan.myapp.adapter.DataTokoAdapter;
import com.example.kurniawan.myapp.helpers.Rest;
import com.example.kurniawan.myapp.helpers.services.APIService;
import com.example.kurniawan.myapp.model.TokoDataModel;
import com.example.kurniawan.myapp.model.TokoListModel;
import com.example.kurniawan.myapp.utils.Utils;
import com.google.gson.Gson;

import interfaces.IAdapterView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TokoActivity extends AppCompatActivity implements IAdapterView{

    private RecyclerView recyclerView;
    private TokoListModel tokoList;
    private DataTokoAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_toko );
        view();
    }

    private void view(){
        adapter = new DataTokoAdapter(null);
        adapter.setAdapterListener(this);

        recyclerView = (RecyclerView) findViewById( R.id.recycletoko );
        recyclerView.setHasFixedSize( true );
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager( getApplicationContext());
        recyclerView.setLayoutManager( layoutManager );
        loadJSON();
    }

    private void itemClick(int pos){
        if(tokoList != null){
            if(tokoList.getData() != null && !tokoList.getData().isEmpty()){
                TokoDataModel data = tokoList.getData().get( pos );

                Intent intent = new Intent( TokoActivity.this, TokoUpdateActivity.class );
                intent.putExtra( "json",(new Gson()).toJson( data ) );
                startActivity( intent );
            }
        }
    }

    private void loadJSON(){
        Rest.make(APIService.class).store().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String jsonString   = Utils.getResponse(response);
                    Gson gson           = new Gson();
                    tokoList            = gson.fromJson(jsonString, TokoListModel.class);

                    if(tokoList != null) {
                        if (tokoList.getData() != null && !tokoList.getData().isEmpty()) {
                            adapter.setList(tokoList.getData());
                            adapter.notifyDataSetChanged();
                            recyclerView.setAdapter( adapter );
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("Error", t.getMessage());
            }
        });
    }


    public void kembali(View view){
        Intent intent = new Intent(TokoActivity.this, AdminActivity.class);
        startActivity(intent);
        finish();
    }

    public void tambahtoko(View view){
        Intent intent = new Intent(TokoActivity.this, Tambahtoko.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void onItemClick(View view, int position) {
        itemClick(position);
    }

    @Override
    public void onLongItemClick(View view, int position) {

    }

}
