package com.example.kurniawan.myapp;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.kurniawan.myapp.helpers.Rest;
import com.example.kurniawan.myapp.helpers.services.APIService;
import com.example.kurniawan.myapp.model.RekomendasiList;
import com.example.kurniawan.myapp.utils.NetworkHelper;
import com.example.kurniawan.myapp.utils.Singleton;
import com.example.kurniawan.myapp.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HomeFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment {

    
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    private LinearLayout llContentCb;
    private List<CheckBox> cbList = new ArrayList<>();
    private String[] labelCbList = {"Musik","Bermain Game","Fotography","Memasak","Membaca","Futsal","Aktivitas Outdoor","Melukis","Travel","Kuliner","Menulis","Basket","Hiking","Film","Sepak Bola","Bersepeda","Berenang"};

    private OnFragmentInteractionListener mListener;

    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    private void showToast(String text){Toast.makeText( getActivity(),text,Toast.LENGTH_SHORT).show();}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView( inflater,container,savedInstanceState );
        View v = inflater.inflate( R.layout.newhome, container, false );
        final ConnectivityManager conMgr = (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        llContentCb = (LinearLayout) v.findViewById(R.id.llContentCb);
        final EditText txt_usia = (EditText)v.findViewById( R.id.txt_usia );
//        final Spinner spn_hobi = (Spinner)v.findViewById( R.id.hobi );
        final RadioGroup gender = (RadioGroup)v.findViewById( R.id.rg_gender);

        Button btnCalculate = (Button) v.findViewById( R.id.btnCalculate );
        btnCalculate.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getCheckboxValue();
                String umur = txt_usia.getText().toString();
                String hobi = getCheckboxValue();
                int selectedId = gender.getCheckedRadioButtonId();
                RadioButton radioButton = (RadioButton)getView().findViewById( selectedId );
                String jk = radioButton.getText().toString();

                if(conMgr.getActiveNetworkInfo() != null
                        && conMgr.getActiveNetworkInfo().isAvailable()
                        && conMgr.getActiveNetworkInfo().isConnected()){
                    showToast(hobi);
                    Hasil(hobi,umur,jk);
                }else{
                    Toast.makeText(getActivity().getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                }

            }
        } );

        loadCheckbox();
        return v;
    }

    private String getCheckboxValue() {
        List<String> labelListSelected = new ArrayList<>();
        String value = "";
        for (int i = 0; i < cbList.size(); i++) {
            CheckBox cbSelected = cbList.get(i);

            if(cbSelected.isChecked()) {
                value += labelCbList[i] + ",";
                labelListSelected.add(labelCbList[i]);
            }
        }

        if(!value.isEmpty())
            value = value.substring(0, (value.length()-1));

//        Toast.makeText(getActivity(), value, Toast.LENGTH_SHORT).show();
        return value;
    }

    private void loadCheckbox() {
        for (int i = 0; i < labelCbList.length; i++) {
            CheckBox checkBox = new CheckBox(getActivity());
            checkBox.setId(i+1);
            checkBox.setText(labelCbList[i]);

            cbList.add(checkBox);
            llContentCb.addView(checkBox);
        }
    }

    private void Hasil(final String hobi, final String umur, final String jk){
        if(NetworkHelper.isConnectedToInternet( Singleton.getInstance().getApplicationContext())){
            Rest.make( APIService.class ).recom(hobi, umur, jk ).enqueue( new Callback <ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        if(response != null & response.isSuccessful()){
                            if(response.code() == 200){
                                String  jsonString      = Utils.getResponse(response);
                                Gson gson               = new Gson();

                                List<RekomendasiList> list  = gson.fromJson(jsonString, new TypeToken<List<RekomendasiList>>() {}.getType());
//                                RekomendasiList vData   = gson.fromJson( jsonString, RekomendasiList.class );

                                if(list != null && !list.isEmpty()){
                                    Intent intent = new Intent(getActivity(), HasilActivity.class);
                                    intent.putExtra( "json",jsonString );
                                    startActivity( intent );
                                }else{
                                    showToast( "Data rekomendasi kosong" );
                                }

                                Log.i("TAG","response123 --" + jsonString);
                            }
                        }

                    }catch (Exception e){
                        e.printStackTrace();
                        showToast( "Gagal Rekomendasi. Silahkan lengkapi form" );
                    }
                }

                @Override
                public void onFailure(Call <ResponseBody> call, Throwable t) {

                    showToast( "Gagal Rekomendasi. Silahkan cek koneksi Anda" );
                }
            } );
        }

    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            Toast.makeText(context, "home", Toast.LENGTH_SHORT);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
