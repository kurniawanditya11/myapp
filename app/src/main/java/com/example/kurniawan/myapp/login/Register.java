package com.example.kurniawan.myapp.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.kurniawan.myapp.LoginAct;
import com.example.kurniawan.myapp.MainActivity;
import com.example.kurniawan.myapp.R;
import com.example.kurniawan.myapp.helpers.Rest;
import com.example.kurniawan.myapp.helpers.services.APIService;
import com.example.kurniawan.myapp.utils.NetworkHelper;
import com.example.kurniawan.myapp.utils.Singleton;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class Register extends AppCompatActivity {

    ProgressDialog pDialog;
    Button btn_register, btn_login;
    EditText txt_email, id_role, txt_username, txt_password, txt_confirm_password;
    Intent intent;

    int success;
    ConnectivityManager conMgr;

//    private String url = Server.BASE_API_URL + "register.php";
//    private static final String TAG = Register.class.getSimpleName();
//
//    private static final String TAG_SUCCESS = "success";
//    private static final String TAG_MESSAGE = "message";
//
//    String tag_json_obj = "json_obj_req";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_register );

        conMgr = (ConnectivityManager) getSystemService( Context.CONNECTIVITY_SERVICE );
        {
            if (conMgr.getActiveNetworkInfo() != null
                    && conMgr.getActiveNetworkInfo().isAvailable()
                    && conMgr.getActiveNetworkInfo().isConnected()) {
            } else {
                Toast.makeText( getApplicationContext(), "No Internet Connection",
                        Toast.LENGTH_LONG ).show();
            }
        }

        btn_register = (Button) findViewById( R.id.btn_register );
        txt_email = (EditText) findViewById( R.id.txt_email);
        txt_username = (EditText) findViewById( R.id.txt_email );
        txt_password = (EditText) findViewById( R.id.txt_password );
        txt_confirm_password = (EditText) findViewById( R.id.txt_confirm_password );

        btn_register.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {
//                 TODO Auto-generated method stub
                String email = txt_email.getText().toString();
                String username = txt_username.getText().toString();
                String password = txt_password.getText().toString();
                String id_role = "1";
                String confirm_password = txt_confirm_password.getText().toString();

                if (conMgr.getActiveNetworkInfo() != null
                        && conMgr.getActiveNetworkInfo().isAvailable()
                        && conMgr.getActiveNetworkInfo().isConnected()) {
                    checkRegister( email, username, password, id_role, confirm_password );
                } else {
                    Toast.makeText( getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT ).show();
                }
            }
        } );
    }

    private void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
    }

    private void checkRegister(
            final String email, final String username, final String password, final String id_role, final String confirm_password) {
        pDialog = new ProgressDialog( this );
        pDialog.setCancelable( false );
        pDialog.setMessage( "Register ..." );
        showDialog();

        if (NetworkHelper.isConnectedToInternet(Singleton.getInstance().getApplicationContext())) {
            Rest.make(APIService.class).register(email, username, password, id_role,  confirm_password).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                    hideDialog();
                    try {
                        if(response != null && response.isSuccessful()) {
                            showToast(response.code() + "\n\n" + "Berhasil Registrasi");
                        } else {
                            showToast(response.code() + "\n\n" + "Gagal Registrasi");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        showToast(0 + "\n\n" + "Gagal Registrasi");
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    showToast(500 + "\n\n" + "Gagal Registrasi");
                }
            });
        }
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    public void onBackPressed() {
        intent = new Intent( Register.this, MainActivity.class );
        finish();
        startActivity( intent );
    }

}