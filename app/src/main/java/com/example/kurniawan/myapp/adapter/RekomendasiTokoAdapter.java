package com.example.kurniawan.myapp.adapter;

import android.location.Location;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.kurniawan.myapp.R;
import com.example.kurniawan.myapp.model.TokoDataModel;
import com.example.kurniawan.myapp.utils.Utils;

import java.text.DecimalFormat;
import java.util.List;

import interfaces.IAdapterView;

public class RekomendasiTokoAdapter extends RecyclerView.Adapter<RekomendasiTokoAdapter.ViewHolder> {
    private List<TokoDataModel> list;
    private IAdapterView adapterListener;
    private Location location; // Untuk get radius on mylocation

    public RekomendasiTokoAdapter(List<TokoDataModel> user){
        this.list = user;
    }

    // Untuk get radius on mylocation
    public void setLocation(Location location) {
        this.location = location;
    }

    public void setList(List<TokoDataModel> list) {
        this.list = list;
    }

    public IAdapterView getAdapterListener() {
        return adapterListener;
    }

    public void setAdapterListener(IAdapterView adapterListener) {
        this.adapterListener = adapterListener;
    }

    @Override
    public RekomendasiTokoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate( R.layout.list_rekomendasi_toko, parent, false );
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RekomendasiTokoAdapter.ViewHolder holder, final int position) {
        holder.tvNama.setText( list.get(position).getNmToko());
        holder.tvAlamat.setText( list.get(position).getAlamat());

        Location target     = new Location("target");

        target.setLatitude(Utils.convertStringToDouble(list.get(position).getLat()));
        target.setLongitude(Utils.convertStringToDouble(list.get(position).getLon()));

        /** Menampilkan radius */
        DecimalFormat formatter = new DecimalFormat("#,##");
        String radius = "";
        if(location != null) {
            float radiusTarget = location.distanceTo(target);
            if(radiusTarget > 1000) {
                radiusTarget = radiusTarget / 1000;
                radius = " (" + formatter.format(radiusTarget) + " KM)";
            } else {
                radius = " (" + formatter.format(radiusTarget) + " Meter)";
            }
        }

        String nama = list.get(position).getAlamat() + radius;
        holder.tvAlamat.setText(nama);
        /** [End] Menampilkan radius */

        holder.btnView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(adapterListener != null) {
                    adapterListener.onItemClick(v, position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvNama;
        private TextView tvAlamat;
        private Button btnView;
        private View root;

        public ViewHolder (View view){
            super(view);

            root = view;
            tvNama = (TextView) view.findViewById( R.id.tvNama );
            tvAlamat = (TextView) view.findViewById( R.id.tvAlamat );
            btnView = (Button) view.findViewById( R.id.btnView );

        }
    }
}
