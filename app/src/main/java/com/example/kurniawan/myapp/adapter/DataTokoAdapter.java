package com.example.kurniawan.myapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.kurniawan.myapp.R;
import com.example.kurniawan.myapp.model.TokoDataModel;

import java.util.List;

import interfaces.IAdapterView;

public class DataTokoAdapter extends RecyclerView.Adapter<DataTokoAdapter.ViewHolder>{
    private List<TokoDataModel>list;
    private IAdapterView adapterListener;

    public DataTokoAdapter(List<TokoDataModel> toko){this.list = toko;}

    public void setList(List<TokoDataModel> list){this.list = list;}

    public IAdapterView getAdapterListener() {
        return adapterListener;
    }

    public void setAdapterListener(IAdapterView adapterListener) {
        this.adapterListener = adapterListener;
    }

    @Override
    public DataTokoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from( parent.getContext()).inflate( R.layout.list_toko, parent, false );
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(DataTokoAdapter.ViewHolder holder, final int position) {
        holder.nmtoko.setText( list.get( position ).getNmToko());
        holder.desc.setText( list.get( position ).getDesc());

        holder.root.setOnClickListener(new View.OnClickListener(){
           @Override
           public void onClick(View v){
               if(adapterListener != null){
                   adapterListener.onItemClick( v, position );
               }
           }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView nmtoko,desc;
        private View root;
        public ViewHolder(View view) {
            super( view);

            root = view;
            nmtoko = (TextView) view.findViewById(R.id.tvnmtoko);
            desc = (TextView) view.findViewById(R.id.tv_desc);
        }
    }
}
