package com.example.kurniawan.myapp.admin;

import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.kurniawan.myapp.R;
import com.example.kurniawan.myapp.helpers.Rest;
import com.example.kurniawan.myapp.helpers.services.APIService;
import com.example.kurniawan.myapp.model.TokoDataModel;
import com.example.kurniawan.myapp.model.UserDataModel;
import com.example.kurniawan.myapp.utils.NetworkHelper;
import com.example.kurniawan.myapp.utils.Singleton;
import com.example.kurniawan.myapp.utils.Utils;
import com.google.gson.Gson;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class TokoUpdateActivity extends AppCompatActivity {
    private EditText txt_idToko,txt_nmToko,txt_lat, txt_lon, txt_desc, txt_alamat;
    private Button btnUpdateToko, btnDeleteToko;
    ConnectivityManager conMgr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_toko_update );
        txt_idToko      = (EditText) findViewById( R.id.tvidtoko );
        txt_nmToko      = (EditText) findViewById( R.id.tvupnamatoko );
        txt_lat         = (EditText) findViewById( R.id.tvuplat );
        txt_lon         = (EditText) findViewById( R.id.tvuplon );
        txt_desc        = (EditText) findViewById( R.id.tvupdesc );
        txt_alamat      = (EditText) findViewById( R.id.tvupalamat );

        btnUpdateToko   = (Button) findViewById( R.id.btnUpdateToko );
        btnDeleteToko   = (Button) findViewById( R.id.btnDeleteToko );
        getBundle();

        btnUpdateToko.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String id           = txt_idToko.getText().toString();
                String nmtoko       = txt_nmToko.getText().toString();
                String lat          = txt_lat.getText().toString();
                String lon          = txt_lon.getText().toString();
                String desc         = txt_desc.getText().toString();
                String alamat       = txt_alamat.getText().toString();
                UpdateToko(id,nmtoko,lat,lon,desc,alamat);
            }
        } );

        btnDeleteToko.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String id           =txt_idToko.getText().toString();
                deleteToko(id);
            }
        } );
    }



    private void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
    }
    private void UpdateToko(String id, String nmtoko, String lat, String lon, String desc, String alamat) {
        if (NetworkHelper.isConnectedToInternet( Singleton.getInstance().getApplicationContext())) {
            Rest.make(APIService.class).updatetoko(id,nmtoko,lat,lon,desc,alamat).enqueue( new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                    try {
                        if(response != null && response.isSuccessful()) {
                            String jsonString = Utils.getResponse(response);
                            JSONObject jsonObj = new JSONObject(jsonString);
                            int status = jsonObj.optInt( "success" );
                            Toast.makeText(TokoUpdateActivity.this,"pesan : "+ jsonObj.optString( "message" ),Toast.LENGTH_LONG).show();
                            if(status == 1) {
                                onBackPressed();
                            }
                        } else {
                            showToast(response.code() + "\n\n" + "Data gagal ditambahkan");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        showToast(0 + "\n\n" + "Data gagal ditambahkan");
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    showToast(500 + "\n\n" + "Data gagal ditambahkan");
                }
            });
        }
    }
    private void deleteToko(String id) {
        if (NetworkHelper.isConnectedToInternet( Singleton.getInstance().getApplicationContext())) {
            Rest.make(APIService.class).deletetoko(id).enqueue( new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                    try {
                        if(response != null && response.isSuccessful()) {
                            String jsonString = Utils.getResponse(response);
                            JSONObject jsonObj = new JSONObject(jsonString);
                            int status = jsonObj.optInt( "success" );
                            Toast.makeText(TokoUpdateActivity.this,"pesan : "+ jsonObj.optString( "message" ),Toast.LENGTH_LONG).show();
                            if(status == 1) {
                                onBackPressed();
                            }
                        } else {
                            showToast(response.code() + "\n\n" + "Data gagal ditambahkan");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        showToast(0 + "\n\n" + "Data gagal ditambahkan");
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    showToast(500 + "\n\n" + "Data gagal ditambahkan");
                }
            });
        }
    }
    private void  getBundle(){
        Bundle extra = getIntent().getExtras();

        if(extra != null){
            String json = extra.getString("json");
            displayData(json);
        }
    }

    private void displayData(String jsonString){
        Gson gson           = new Gson();
        TokoDataModel vData = gson.fromJson( jsonString, TokoDataModel.class );

        txt_idToko.setText(vData.getIdToko());
        txt_nmToko.setText( vData.getNmToko() );
        txt_lat.setText( vData.getLat() );
        txt_lon.setText( vData.getLon() );
        txt_desc.setText( vData.getDesc() );
        txt_alamat.setText( vData.getAlamat() );


    }
}
