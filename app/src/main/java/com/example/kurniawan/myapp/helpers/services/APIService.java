package com.example.kurniawan.myapp.helpers.services;


import com.example.kurniawan.myapp.response.ResponseRoute;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Yudi Rahmat
 * Email yudirahmat7@gmail.com
 */

public interface APIService {
    @GET("v2/user/membership")
    Call<ResponseBody> member(@Header("Authorization") String header);

//    @FormUrlEncoded
    @POST("login.php")
    Call<ResponseBody> login(@Field("email") String email, @Field( "password" ) String password);

    @FormUrlEncoded
    @POST("register.php")
    Call<ResponseBody> register(@Field("email") String email,
                                @Field("username") String username,
                                @Field("password") String password,
                                @Field("id_role") String id_role,
                                @Field("confirm_password") String confirm_password);

    // Ambil data toko
    @GET("toko.php?aksi=getDataToko")
    Call<ResponseBody> store();

    // Simpan data toko
    @FormUrlEncoded
    @POST("tambahtoko.php")
    Call<ResponseBody> addstore(@Field( "nm_toko" ) String nm_toko,
                                @Field( "lattitude" ) String lattitude,
                                @Field( "longitude" ) String longitude,
                                @Field( "deskripsi" ) String deskripsi,
                                @Field( "alamat" ) String alamat);

    // Ambil data user
    @GET("user.php?aksi=getDataUser")
    Call<ResponseBody> users();

    // Simpan data user
    @FormUrlEncoded
    @POST("tambahuser.php")
    Call<ResponseBody> adduser(@Field( "email" ) String email,
                               @Field( "username" ) String username,
                                @Field( "password" ) String password);

    //Update data user
    @FormUrlEncoded
    @POST("updateuser.php")
    Call<ResponseBody> updateuser(@Field( "id" ) String id,
                                  @Field( "email" ) String email,
                                  @Field( "username" ) String username,
                                  @Field( "password" ) String password);

    //Delete data user
    @FormUrlEncoded
    @POST("hapususer.php")
    Call<ResponseBody> deleteuser(@Field( "id" ) String id);


    //Update data user
    @FormUrlEncoded
    @POST("updatetoko.php")
    Call<ResponseBody> updatetoko(@Field( "id" ) String id,
                                  @Field( "nm_toko" ) String nm_toko,
                                  @Field( "lat" ) String lat,
                                  @Field( "lon" ) String lon,
                                  @Field( "desc" ) String desc,
                                  @Field( "alamat" ) String alamat);

    //Delete data user
    @FormUrlEncoded
    @POST("hapustoko.php")
    Call<ResponseBody> deletetoko(@Field( "id" ) String id);


    //Caritoko
    @FormUrlEncoded
    @POST("toko.php?aksi=caritoko.php")
    Call<ResponseBody> caritoko(@Field("cari") String cari);

    //getdatasesuaikategori
    @FormUrlEncoded
    @POST("toko.php?aksi=tampildatasesuailokasi")
    Call<ResponseBody> tampiltoko(@Field("kat") int kat);

    // Ambil data rekomendasi
    @GET("recomen.php?aksi=getDataRekomendasi")
    Call<ResponseBody> rekomendasi();

    @FormUrlEncoded
    @POST("recom.php")
    Call<ResponseBody> recom(@Field("hobi") String hobi,
                             @Field("umur") String umur,
                             @Field("jk") String jk);


    // Ambil data hobi
    @GET("hobi.php")
    Call<ResponseBody> hobi();

//    @GET("rekomendasi/toko.php?aksi=getDataToko")
//    Call<JSONResponse> getJSON();



    //https://maps.googleapis.com/maps/api/directions/
    // json?origin=Cirebon,ID&destination=Jakarta,ID&api_key=YOUR_API_KEY
    @GET("json")
    Call<ResponseRoute> request_route(
            @Query("origin") String origin,
            @Query("destination") String destination,
            @Query("api_key") String api_key
    );

}
