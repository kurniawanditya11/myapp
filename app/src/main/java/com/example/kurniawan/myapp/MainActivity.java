package com.example.kurniawan.myapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.kurniawan.myapp.login.Register;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void kelogin(View view) {
        Intent intent = new Intent(MainActivity.this, LoginAct.class);
        startActivity(intent);
    }

    public void keregister(View view) {
        Intent intent = new Intent(MainActivity.this, BottomActivity.class);
        startActivity(intent);
    }
}
