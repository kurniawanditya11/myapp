package com.example.kurniawan.myapp.admin;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.kurniawan.myapp.R;
import com.example.kurniawan.myapp.adapter.DataUsersAdapter;
import com.example.kurniawan.myapp.helpers.Rest;
import com.example.kurniawan.myapp.helpers.services.APIService;
import com.example.kurniawan.myapp.model.UserDataModel;
import com.example.kurniawan.myapp.model.UserListModel;
import com.example.kurniawan.myapp.utils.Utils;
import com.google.gson.Gson;

import interfaces.IAdapterView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserActivity extends AppCompatActivity implements IAdapterView {

    private RecyclerView recyclerView;
    private UserListModel userList;
    private DataUsersAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_user );
        view();
    }

    private void view() {
        adapter = new DataUsersAdapter(null);
        adapter.setAdapterListener(this);

        recyclerView = (RecyclerView) findViewById( R.id.recycleuser );
        recyclerView.setHasFixedSize( true );
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager( getApplicationContext() );
        recyclerView.setLayoutManager( layoutManager );
        loadJSON();
    }

    private void itemClick(int pos){
        if(userList != null) {
            if (userList.getData() != null && !userList.getData().isEmpty()) {
                UserDataModel data = userList.getData().get(pos);

//                String username = data.getUserName();
//                String email    = data.getEmail();

                Intent intent = new Intent(UserActivity.this, UserUpdateActivity.class);

                intent.putExtra( "json", (new Gson()).toJson( data ) );

                startActivity( intent );
//                showToast( email);
            }
        }
    }

    private void loadJSON() {
        Rest.make( APIService.class).users().enqueue( new Callback <ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try{
                    final String jsonString   = Utils.getResponse( response );
                    Gson gson                 = new Gson();
                    userList                  = gson.fromJson(jsonString, UserListModel.class);

                    if(userList != null){
                        if(userList.getData() != null && !userList.getData().isEmpty()){
                            adapter.setList(userList.getData());
                            adapter.notifyDataSetChanged();
                            recyclerView.setAdapter( adapter );
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call <ResponseBody> call, Throwable t) {
                Log.d("Error", t.getMessage());
            }
        } );
    }


    public void kembali(View view){
        Intent intent = new Intent(UserActivity.this, AdminActivity.class);
        startActivity(intent);
        finish();
    }
    public void tambahuser(View view){
        Intent intent = new Intent(UserActivity.this, Tambahuser.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {

    }


    @Override
    public void onItemClick(View view, int position) {
        itemClick(position);
    }

    @Override
    public void onLongItemClick(View view, int position) {

    }
}
