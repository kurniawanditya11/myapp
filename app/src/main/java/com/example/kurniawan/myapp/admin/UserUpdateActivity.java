package com.example.kurniawan.myapp.admin;

import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.kurniawan.myapp.R;
import com.example.kurniawan.myapp.helpers.Rest;
import com.example.kurniawan.myapp.helpers.services.APIService;
import com.example.kurniawan.myapp.model.UserDataModel;
import com.example.kurniawan.myapp.utils.NetworkHelper;
import com.example.kurniawan.myapp.utils.Singleton;
import com.example.kurniawan.myapp.utils.Utils;
import com.google.gson.Gson;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class UserUpdateActivity extends AppCompatActivity {

    private EditText txt_id,txt_email, txt_username, txt_password;
    private Button btnUpdateUser, btnDeleteUser;
    ConnectivityManager conMgr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_user_update );
        txt_id          = (EditText) findViewById( R.id.tvid );
        txt_email       = (EditText) findViewById( R.id.tvupemail );
        txt_username    = (EditText) findViewById( R.id.tvupusername );
        txt_password    = (EditText) findViewById( R.id.tvuppassword );
        btnUpdateUser   = (Button) findViewById( R.id.btnUpdateUser );
        btnDeleteUser   = (Button) findViewById( R.id.btnDeleteUser );
        getBundle();

        btnUpdateUser.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String id       = txt_id.getText().toString();
                String email    = txt_email.getText().toString();
                String username = txt_username.getText().toString();
                String password = txt_password.getText().toString();
                updateUser(id,email,username,password);
            }
        } );

        btnDeleteUser.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String id    = txt_id.getText().toString();
                deleteUser(id);
            }
        } );
    }

    private void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
    }

    private void updateUser(final String id, final String email, final String username, final String password) {
        if (NetworkHelper.isConnectedToInternet( Singleton.getInstance().getApplicationContext())) {
            Rest.make(APIService.class).updateuser(id,email,username,password).enqueue( new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                    try {
                        if(response != null && response.isSuccessful()) {
                            String jsonString = Utils.getResponse(response);
                            JSONObject jsonObj = new JSONObject(jsonString);
                            int status = jsonObj.optInt( "success" );
                            Toast.makeText(UserUpdateActivity.this,"pesan : "+ jsonObj.optString( "message" ),Toast.LENGTH_LONG).show();
                            if(status == 1) {
                                onBackPressed();
                            }
                        } else {
                            showToast(response.code() + "\n\n" + "Data gagal ditambahkan");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        showToast(0 + "\n\n" + "Data gagal ditambahkan");
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    showToast(500 + "\n\n" + "Data gagal ditambahkan");
                }
            });
        }
    }

    private void deleteUser(final String id) {
        if (NetworkHelper.isConnectedToInternet( Singleton.getInstance().getApplicationContext())) {
            Rest.make(APIService.class).deleteuser(id).enqueue( new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                    try {
                        if(response != null && response.isSuccessful()) {
                            String jsonString = Utils.getResponse(response);
                            JSONObject jsonObj = new JSONObject(jsonString);
                            int status = jsonObj.optInt( "success" );
                            Toast.makeText(UserUpdateActivity.this,"pesan : "+ jsonObj.optString( "message" ),Toast.LENGTH_LONG).show();
                            if(status == 1) {
                                onBackPressed();
                            }
                        } else {
                            showToast(response.code() + "\n\n" + "Data gagal ditambahkan");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        showToast(0 + "\n\n" + "Data gagal ditambahkan");
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    showToast(500 + "\n\n" + "Data gagal ditambahkan");
                }
            });
        }
    }




    private void  getBundle(){
        Bundle extra = getIntent().getExtras();

        if(extra != null){
            String json = extra.getString("json");
            displayData(json);
        }
    }

    private void displayData(String jsonString){
        Gson gson           = new Gson();
        UserDataModel vData = gson.fromJson( jsonString,UserDataModel.class );

        txt_id.setText(vData.getId());
        txt_email.setText( vData.getEmail() );
        txt_username.setText( vData.getUserName() );
        txt_password.setText( vData.getPassWord() );


    }
}
