package com.example.kurniawan.myapp.admin;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.example.kurniawan.myapp.R;
import com.example.kurniawan.myapp.adapter.DataRekomendasiAdapter;
import com.example.kurniawan.myapp.helpers.Rest;
import com.example.kurniawan.myapp.helpers.services.APIService;
import com.example.kurniawan.myapp.model.RekomendasiListModel;
import com.example.kurniawan.myapp.utils.Utils;
import com.google.gson.Gson;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RekomendasiActivity extends AppCompatActivity{
    private RecyclerView recyclerView;
    private RekomendasiListModel rekomendasiList;
    private DataRekomendasiAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_rekomendasi);
        view();
    }

    private void view() {
        recyclerView = (RecyclerView) findViewById( R.id.recyclerekomendasi );
        recyclerView.setHasFixedSize( true );
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager( getApplicationContext() );
        recyclerView.setLayoutManager( layoutManager );
        loadJSON();
    }

    private void loadJSON() {
        Rest.make( APIService.class).rekomendasi().enqueue( new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try{
                    String jsonString   = Utils.getResponse( response );
                    Gson gson           = new Gson();
                    rekomendasiList     = gson.fromJson(jsonString, RekomendasiListModel.class);

                    if(rekomendasiList != null){
                        if(rekomendasiList.getData() != null && !rekomendasiList.getData().isEmpty()){
                            adapter = new DataRekomendasiAdapter(rekomendasiList.getData());
                            recyclerView.setAdapter( adapter );

                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call <ResponseBody> call, Throwable t) {
                Log.d("Error", t.getMessage());
            }
        } );
    }


    public void kembali(View view){
        Intent intent = new Intent(RekomendasiActivity.this, AdminActivity.class);
        startActivity(intent);
        finish();
    }
}
