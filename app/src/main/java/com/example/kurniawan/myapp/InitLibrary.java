package com.example.kurniawan.myapp;

import com.example.kurniawan.myapp.helpers.services.APIService;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class InitLibrary {
    //https://maps.googleapis.com/maps/api/directions/json?origin=Cirebon,ID&destination=Jakarta,ID&api_key=YOUR_API_KEY
    public static String BASE_URL = "https://maps.googleapis.com/maps/api/directions/";
    public static Retrofit setInit(){
        return new Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory( GsonConverterFactory.create())
                .build();
    }
    public static APIService getInstance(){
        return setInit().create(APIService.class);
    }
}
