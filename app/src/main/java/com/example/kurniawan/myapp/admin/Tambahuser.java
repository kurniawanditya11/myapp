package com.example.kurniawan.myapp.admin;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.kurniawan.myapp.R;
import com.example.kurniawan.myapp.helpers.Rest;
import com.example.kurniawan.myapp.helpers.services.APIService;
import com.example.kurniawan.myapp.utils.NetworkHelper;
import com.example.kurniawan.myapp.utils.Singleton;
import com.example.kurniawan.myapp.utils.Utils;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class Tambahuser extends AppCompatActivity {
    EditText txt_email,txt_username,txt_password;
    Button btnAddUser;
    Intent intent;

    ConnectivityManager conMgr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_tambahuser );
        conMgr = (ConnectivityManager) getSystemService( Context.CONNECTIVITY_SERVICE );
        {
            if (conMgr.getActiveNetworkInfo() != null
                    && conMgr.getActiveNetworkInfo().isAvailable()
                    && conMgr.getActiveNetworkInfo().isConnected()) {
            } else {
                Toast.makeText( getApplicationContext(), "No Internet Connection",
                        Toast.LENGTH_LONG ).show();
            }
        }

        txt_email = (EditText) findViewById( R.id.tv_email);
        txt_username = (EditText) findViewById( R.id.tv_username );
        txt_password = (EditText) findViewById( R.id.tv_password );

        btnAddUser = (Button) findViewById( R.id.btnAddUser );
        btnAddUser.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                String email = txt_email.getText().toString();
                String username = txt_username.getText().toString();
                String password = txt_password.getText().toString();

                if (conMgr.getActiveNetworkInfo() != null
                        && conMgr.getActiveNetworkInfo().isAvailable()
                        && conMgr.getActiveNetworkInfo().isConnected()) {
                    addUser( email,username,password );
                } else {
                    Toast.makeText( getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT ).show();
                }
            }
        } );
    }
    private void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
    }

    private void addUser(final String email,final String username,final String password){
        if (NetworkHelper.isConnectedToInternet( Singleton.getInstance().getApplicationContext())) {
            Rest.make(APIService.class).adduser(email,username,password).enqueue( new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                    try {
                        if(response != null && response.isSuccessful()) {
                            String jsonString = Utils.getResponse(response);
                            JSONObject jsonObj = new JSONObject(jsonString);
                            int status = jsonObj.optInt( "success" );
                            Toast.makeText(Tambahuser.this,"pesan : "+ jsonObj.optString( "message" ),Toast.LENGTH_LONG).show();
                            if(status == 1) {
                                onBackPressed();
                            }
                        } else {
                            showToast(response.code() + "\n\n" + "Data gagal ditambahkan");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        showToast(0 + "\n\n" + "Data gagal ditambahkan");
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    showToast(500 + "\n\n" + "Data gagal ditambahkan");
                }
            });
        }
    }
    @Override
    public void onBackPressed() {
        intent = new Intent( Tambahuser.this, UserActivity.class );
        startActivity( intent );
        finish();
    }
}
