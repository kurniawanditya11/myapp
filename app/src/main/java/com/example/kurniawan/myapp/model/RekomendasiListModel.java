package com.example.kurniawan.myapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RekomendasiListModel {
    @SerializedName( "data" )
    @Expose
    private List<RekomendasiDataModel> data = null;

    public List <RekomendasiDataModel> getData() {
        return data;
    }

    public void setData(List<RekomendasiDataModel> data){
        this.data = data;
    }
}
