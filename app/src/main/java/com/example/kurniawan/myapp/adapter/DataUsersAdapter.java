package com.example.kurniawan.myapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.kurniawan.myapp.R;
import com.example.kurniawan.myapp.model.UserDataModel;

import java.util.List;

import interfaces.IAdapterView;

public class DataUsersAdapter extends RecyclerView.Adapter<DataUsersAdapter.ViewHolder> {
    private List<UserDataModel> list;
    private IAdapterView adapterListener;

    public DataUsersAdapter(List<UserDataModel> user){
        this.list = user;
    }

    public void setList(List<UserDataModel> list) {
        this.list = list;
    }

    public IAdapterView getAdapterListener() {
        return adapterListener;
    }

    public void setAdapterListener(IAdapterView adapterListener) {
        this.adapterListener = adapterListener;
    }

    @Override
    public DataUsersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate( R.layout.list_user, parent, false );
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(DataUsersAdapter.ViewHolder holder, final int position) {
        holder.username.setText( list.get(position).getUserName());
        holder.email.setText( list.get(position).getEmail());

        holder.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(adapterListener != null) {
                    adapterListener.onItemClick(v, position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView username;
        private TextView email;
        private View root;

        public ViewHolder (View view){
            super(view);

            root = view;
            username = (TextView) view.findViewById( R.id.tvinterest );
            email = (TextView) view.findViewById( R.id.tvemail );
        }
    }
}
