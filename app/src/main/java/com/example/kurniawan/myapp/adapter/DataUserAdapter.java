package com.example.kurniawan.myapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.example.kurniawan.myapp.R;
import com.example.kurniawan.myapp.model.UserDataModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DataUserAdapter extends RecyclerView.Adapter<DataUserAdapter.ViewHolder>{
    public interface OnItemClickListener {
        void onItemClick(UserDataModel item);
    }

    private final List<UserDataModel> luser;
    private final OnItemClickListener listener;

    public DataUserAdapter(List<UserDataModel> luser, OnItemClickListener listener) {
        this.luser = luser;
        this.listener = listener;
    }

    @Override
    public DataUserAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate( R.layout.list_user, parent, false );
        return new DataUserAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(DataUserAdapter.ViewHolder holder, int position) {
        holder.bind(luser.get(position), listener);
//        holder.username.setText( luser.get(position).getUsername());
//        holder.password.setText( luser.get(position).getPassword());

    }


    @Override
    public int getItemCount() {return luser.size(); }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView username, password;
        public ViewHolder (View view){
            super(view);
              username= (TextView) view.findViewById( R.id.tvinterest );
              password= (TextView) view.findViewById( R.id.tvhadiah );
        }

        public void bind(UserDataModel userDataModel, OnItemClickListener listener) {
//            username.setText( userDataModel.getUsername());
        }
    }

}
