package com.example.kurniawan.myapp.model;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Rekomendasinew  {

    @SerializedName("umur")
    @Expose
    private String umur;
    @SerializedName("jk")
    @Expose
    private String jk;
    @SerializedName("hobi")
    @Expose
    private List<String> hobi = null;

    public String getUmur() {
        return umur;
    }

    public void setUmur(String umur) {
        this.umur = umur;
    }

    public String getJk() {
        return jk;
    }

    public void setJk(String jk) {
        this.jk = jk;
    }

    public List<String> getHobi() {
        return hobi;
    }

    public void setHobi(List<String> hobi) {
        this.hobi = hobi;
    }

    public String toString() {
        return "Post{" +
                "umur='" + umur + '\'' +
                ", jk='" + jk + '\'' +
                ", hobi=" + '[' + hobi + ']'+
                '}';
    }

}

