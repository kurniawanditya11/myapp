package com.example.kurniawan.myapp.utils;

import android.content.SharedPreferences;
import android.support.multidex.MultiDexApplication;

/**
 * Created by Yudi Rahmat
 * Email yudirahmat7@gmail.com
 */

public class Singleton extends MultiDexApplication {

    public static Singleton singleton;
    public static String packageName;
    public static String authToken;

    public static Singleton getInstance() {
        return singleton;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        singleton   = this;
        packageName = getPackageName();
    }

    public SharedPreferences getSP() {
        return this.getSharedPreferences(packageName, MODE_PRIVATE);
    }

    public String getAuthToken() {
        if (authToken == null) {
            authToken = this.getSP().getString("api_token", "none");
        }
        return authToken;
    }

    public void setAuthToken(String token) {
        authToken = token;
        this.getSP().edit().putString("api_token", token).apply();
    }
}
