package com.example.kurniawan.myapp;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kurniawan.myapp.adapter.DataUsersAdapter;
import com.example.kurniawan.myapp.adapter.RekomendasiTokoAdapter;
import com.example.kurniawan.myapp.model.RekomendasiList;
import com.example.kurniawan.myapp.model.TokoDataModel;
import com.example.kurniawan.myapp.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import interfaces.IAdapterView;
import okhttp3.internal.Util;

public class HasilActivity extends AppCompatActivity implements IAdapterView {

    private RecyclerView recyclerView;
    private TextView tvHasil;
    private TextView tvAlamat;
    private Button btnBack;
    public String namaToko, alamatToko;
    double lonToko, latToko;

    private RekomendasiTokoAdapter mAdapterToko;
    private List<TokoDataModel> mToko;

    LocationManager locationManager;
    Location myLocation;

    double latitude1, longitude1,latitude2, longitude2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hasilrekomendasi);

        recyclerView = (RecyclerView) findViewById( R.id.recycleuser );
        tvHasil = (TextView) findViewById(R.id.txt_recomendation);
        tvAlamat = (TextView) findViewById(R.id.txt_rekomendasitoko);
        btnBack = (Button) findViewById( R.id.button2 );
        btnBack.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HasilActivity.this, BottomActivity.class);
                startActivity( intent );
                finish();
            }
        } );

        requestLocation();
        setTokoAdapter();
        getBundle();
    }

    private void getBundle() {
        Bundle extras = getIntent().getExtras();

        if(extras != null) {
            String json = extras.getString("json");

            displayData(json);
        }
    }

    private void requestLocation() {
        locationManager = (LocationManager) HasilActivity.this.getSystemService( Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(HasilActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(HasilActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates( LocationManager.GPS_PROVIDER, 2000, 10, locationListener );

        myLocation = locationManager.getLastKnownLocation( LocationManager.PASSIVE_PROVIDER );
        longitude2 = myLocation.getLongitude();
        latitude2 = myLocation.getLatitude();
    }

    private void setTokoAdapter() {
        mAdapterToko = new RekomendasiTokoAdapter(null);
        mAdapterToko.setAdapterListener(this);
        mAdapterToko.setLocation(myLocation); // Untuk get radius on mylocation

        recyclerView.setHasFixedSize( true );
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager( getApplicationContext() );
        recyclerView.setLayoutManager( layoutManager );
    }

    private void openMaps(int position) {
        if(mToko == null) return;
        if(position > mToko.size()) return;

        if(mToko != null && !mToko.isEmpty()) {
            TokoDataModel toko = mToko.get(position);

//            Toast.makeText(HasilActivity.this, toko.getNmToko() + " - "+ toko.getLat(), Toast.LENGTH_SHORT).show();
            try {
//                String uri = String.format( Locale.ENGLISH, "http://maps.google.com/maps?saddr=%f,%f(%s)&daddr=%f,%f (%s)", latitude2, longitude2, "My Location", latToko, lonToko , namaToko);
//                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
//                intent.setPackage("com.google.android.apps.maps");
//                startActivity(intent);
                    Intent intent = new Intent(HasilActivity.this, MapsActivity.class );
                    intent.putExtra( "mylat",latitude2);
                    intent.putExtra( "mylon",longitude2);
                    intent.putExtra( "lokasilat",latToko);
                    intent.putExtra( "lokasilon",lonToko);
                    startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void displayData(String jsonString) {
        Gson gson                   = new Gson();
        List<RekomendasiList> list  = gson.fromJson(jsonString, new TypeToken<List<RekomendasiList>>() {}.getType());
//        RekomendasiList vData       = gson.fromJson(jsonString, RekomendasiList.class);

        List<String> mList          = new ArrayList<>();
        mToko                       = new ArrayList<>();

        for (int i = 0; i< list.size(); i++) {
            mList.addAll(list.get(i).getData());
            mToko.addAll(list.get(i).getToko());
        }

        if(mList != null && !mList.isEmpty()) {
            String data = "";
            for (int i = 0; i < mList.size(); i++) {
                data    += mList.get(i) + "\n";
            }

            Utils.replaceChar(data, ",", "\n");
            tvHasil.setText(data);
        }

        if(mToko != null && !mToko.isEmpty()) {
            mAdapterToko.setLocation(myLocation); // untuk radius on mylocation
            mAdapterToko.setList(mToko);
            mAdapterToko.notifyDataSetChanged();
            recyclerView.setAdapter( mAdapterToko );

            String data = "";
            for (int i = 0; i < mToko.size(); i++) {
                TokoDataModel toko = mToko.get(i);
                data    += toko.getNmToko() + "\n " + toko.getAlamat() + "\n";
                namaToko = toko.getNmToko();
                alamatToko = toko.getAlamat();
                latToko = Double.parseDouble( toko.getLat() );
                lonToko = Double.parseDouble( toko.getLon() );
            }

            tvAlamat.setText(data);

            tvAlamat.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String uri = String.format( Locale.ENGLISH, "http://maps.google.com/maps?saddr=%f,%f(%s)&daddr=%f,%f (%s)", latitude2, longitude2, "My Location", latToko, lonToko , namaToko);
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                    intent.setPackage("com.google.android.apps.maps");
                    startActivity(intent);



                }
            } );
        }

    }
    private final LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            longitude1 = location.getLongitude();
            latitude1 = location.getLatitude();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    @Override
    public void onItemClick(View view, int position) {
        openMaps(position);
    }

    @Override
    public void onLongItemClick(View view, int position) {

    }
}


