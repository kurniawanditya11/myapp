package com.example.kurniawan.myapp.admin;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.kurniawan.myapp.LoginAct;
import com.example.kurniawan.myapp.R;

public class AdminActivity extends AppCompatActivity {
    ImageView btnLogout;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_adminpage );
    }

    @Override
    public void onBackPressed() {

    }
    public void datarekomendasi(View view){
        Intent intent = new Intent(AdminActivity.this, RekomendasiActivity.class);
        startActivity(intent);
        finish();
    }

    public void tampiluser(View view) {
        Intent intent = new Intent(AdminActivity.this, UserActivity.class);
        startActivity(intent);
        finish();
    }

    public void tampiltoko(View view){
        Intent intent = new Intent(AdminActivity.this, TokoActivity.class);
        startActivity(intent);
        finish();
    }
    public void tambahtoko(View view){
        Intent intent = new Intent(AdminActivity.this, Tambahtoko.class);
        startActivity(intent);
        finish();
    }
    public void keluar (View view){
        Intent i = new Intent(AdminActivity.this, LoginAct.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();

    }
}

