package com.example.kurniawan.myapp;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.TextView;

import com.example.kurniawan.myapp.helpers.services.APIService;
import com.example.kurniawan.myapp.response.Distance;
import com.example.kurniawan.myapp.response.Duration;
import com.example.kurniawan.myapp.response.LegsItem;
import com.example.kurniawan.myapp.response.ResponseRoute;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

//    private LatLng pickUpLatLng = new LatLng(-6.175110, 106.865039);
//    private LatLng locationLatLng = new LatLng(-6.884346,107.544598);

    private String API_KEY = "AIzaSyD7CKHJsW3dd87ZPLC04roDml0LpBfwY5s";

    private TextView tvStartAddress, tvEndAddress, tvDuration, tvDistance;
    double myLat, myLon, lokasiLat, lokasiLon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_maps2 );
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById( R.id.map );
        mapFragment.getMapAsync( this );

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            myLat = extras.getDouble("mylat");
            myLon = extras.getDouble("mylon");
            lokasiLat = extras.getDouble("lokasilat");
            lokasiLon = extras.getDouble("lokasilon");

        }

        actionRoute();
        widgetInit();
    }

    private void widgetInit() {
        tvStartAddress = findViewById(R.id.tvStartAddress);
        tvEndAddress = findViewById(R.id.tvEndAddress);
        tvDuration = findViewById(R.id.tvDuration);
        tvDistance = findViewById(R.id.tvDistance);
    }

    private void actionRoute(){
        final LatLng pickUpLatLng = new LatLng(myLat, myLon);
        final LatLng locationLatLng = new LatLng(lokasiLat,lokasiLon);
        String lokasiAwal = pickUpLatLng.latitude + "," + pickUpLatLng.longitude;
        String lokasiAkhir = locationLatLng.latitude + "," + locationLatLng.longitude;


        // Panggil Retrofit
        final APIService api = InitLibrary.getInstance();
        // Siapkan request
        final retrofit2.Call<ResponseRoute> routeRequest = api.request_route(lokasiAwal, lokasiAkhir, API_KEY);
        routeRequest.enqueue( new Callback <ResponseRoute>() {
            @Override
            public void onResponse(Call <ResponseRoute> call, Response <ResponseRoute> response) {
                if (response.isSuccessful()){
                    ResponseRoute dataDirection = response.body();

                    LegsItem dataLegs = dataDirection.getRoutes().get( 0 ).getLegs().get( 0 );
                    String polylinePoint = dataDirection.getRoutes().get( 0 ).getOverviewPolyline().getPoints();
                    List<LatLng> decodePath = PolyUtil.decode( polylinePoint );
                    mMap.addPolyline( new PolylineOptions().addAll( decodePath ).width( 8f )
                    .color( Color.argb( 255, 56, 167, 252 ))).setGeodesic(true);
                    mMap.addMarker( new MarkerOptions().position( pickUpLatLng ).title("Lokasi Anda") );
                    mMap.addMarker( new MarkerOptions().position( locationLatLng ).title("Lokasi Akhir") );
                    tvStartAddress.setText("start location : " + dataLegs.getStartAddress().toString());
                    tvEndAddress.setText("end location : " + dataLegs.getEndAddress().toString());
                    Distance dataDistance = dataLegs.getDistance();
                    Duration dataDuration = dataLegs.getDuration();

                    tvDistance.setText("distance : " + dataDistance.getText() + " (" + dataDistance.getValue() + ")");
                    tvDuration.setText("duration : " + dataDuration.getText() + " (" + dataDuration.getValue() + ")");

                    LatLngBounds.Builder latLongBuilder = new LatLngBounds.Builder();
                    latLongBuilder.include(pickUpLatLng);
                    latLongBuilder.include(locationLatLng);

                    LatLngBounds bounds = latLongBuilder.build();

                    int width = getResources().getDisplayMetrics().widthPixels;
                    int height = getResources().getDisplayMetrics().heightPixels;
                    int paddingMap = (int) (width * 0.2); //jarak dari
                    CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, paddingMap);
                    mMap.animateCamera(cu);

                }
            }

            @Override
            public void onFailure(Call <ResponseRoute> call, Throwable t) {
                t.printStackTrace();
            }
        } );
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

//        // Add a marker in Sydney and move the camera
//        LatLng sydney = new LatLng( -34, 151 );
//        mMap.addMarker( new MarkerOptions().position( sydney ).title( "Marker in Sydney" ) );
//        mMap.moveCamera( CameraUpdateFactory.newLatLng( sydney ) );
    }
}
