package com.example.kurniawan.myapp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kurniawan.myapp.adapter.DataTokoAdapter;
import com.example.kurniawan.myapp.adapter.DataTokoNearAdapter;
import com.example.kurniawan.myapp.helpers.Rest;
import com.example.kurniawan.myapp.helpers.services.APIService;
import com.example.kurniawan.myapp.model.TokoDataModel;
import com.example.kurniawan.myapp.model.TokoListModel;
import com.example.kurniawan.myapp.utils.Constant;
import com.example.kurniawan.myapp.utils.Utils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import java.util.List;
import java.util.Locale;

import interfaces.IAdapterView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class MFragment extends Fragment implements OnMapReadyCallback,IAdapterView {
    private GoogleMap map;

    public TokoListModel tokoList;
    private DataTokoNearAdapter adapter;
    private TextView hasil;
    private RecyclerView recyclerView;
    private List<TokoDataModel> mToko;
    Location myLocation;
    double lonToko, latToko;
    double latitude1, longitude1,latitude2, longitude2;
    LocationManager locationManager;

    int MY_LOCATION_REQUEST_CODE = 90;

    double latitude, longitude;

    public MFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_m, container, false);
    }

    private void initMaps() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            if (map != null) map.setMyLocationEnabled(true);
        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.READ_CONTACTS},
                    MY_LOCATION_REQUEST_CODE);
        }

        if (map != null)
            map.getUiSettings().setZoomControlsEnabled(true);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map1);
        mapFragment.getMapAsync(this);

        initData();
        initMaps();
        view();
        loadJSON();

    }
    private void requestLocation() {
        locationManager = (LocationManager) getActivity().getSystemService( Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates( LocationManager.GPS_PROVIDER, 2000, 10, locationListener );

        myLocation = locationManager.getLastKnownLocation( LocationManager.PASSIVE_PROVIDER );
        longitude2 = myLocation.getLongitude();
        latitude2 = myLocation.getLatitude();
    }

    private void view(){
        adapter = new DataTokoNearAdapter(null);
        adapter.setAdapterListener(this);
        adapter.setLocation( myLocation );

        recyclerView = (RecyclerView) getActivity().findViewById( R.id.recyclelistoko);
        recyclerView.setHasFixedSize( true );
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager( getActivity().getApplicationContext());
        recyclerView.setLayoutManager( layoutManager );
        loadJSON();
    }
    private void openMaps(int position) {
        if(mToko == null) return;
        if(position > mToko.size()) return;

        if(mToko != null && !mToko.isEmpty()) {
            TokoDataModel toko = mToko.get(position);

//            Toast.makeText(HasilActivity.this, toko.getNmToko() + " - "+ toko.getLat(), Toast.LENGTH_SHORT).show();
            try {
//                String uri = String.format( Locale.ENGLISH, "http://maps.google.com/maps?saddr=%f,%f(%s)&daddr=%f,%f (%s)", latitude2, longitude2, "My Location", latToko, lonToko , namaToko);
//                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
//                intent.setPackage("com.google.android.apps.maps");
//                startActivity(intent);
                Intent intent = new Intent(getActivity(), MapsActivity.class );
                intent.putExtra( "mylat",latitude2);
                intent.putExtra( "mylon",longitude2);
                intent.putExtra( "lokasilat",latToko);
                intent.putExtra( "lokasilon",lonToko);
                startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void initData() {
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        Button btnHadiah = (Button) getActivity().findViewById(R.id.btnHadiah);
        btnHadiah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                locationManager.requestLocationUpdates( LocationManager.GPS_PROVIDER, 2000, 10, locationListener );
                Location myLocation = locationManager.getLastKnownLocation( LocationManager.PASSIVE_PROVIDER );
                double longitude = myLocation.getLongitude();
                double latitude = myLocation.getLatitude();
                
                LatLng latLng = new LatLng(latitude, longitude);
                Circle circle = map.addCircle(new CircleOptions()
                        .center(latLng)
                        .radius(3000)
                        .strokeWidth(1)
                        .strokeColor(Color.WHITE)
                        .fillColor(Color.argb(50, 54, 161, 196))
                        .clickable(true));

                createMarker(1);
                if (map != null)
//                    map.moveCamera( CameraUpdateFactory.newLatLngZoom( latLng, 20 ) );
                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 11));
            }
        });

        Button btnPesta = (Button) getActivity().findViewById(R.id.btnPesta);
        btnPesta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                locationManager.requestLocationUpdates( LocationManager.GPS_PROVIDER, 2000, 10, locationListener );
                Location myLocation = locationManager.getLastKnownLocation( LocationManager.PASSIVE_PROVIDER );
                double longitude = myLocation.getLongitude();
                double latitude = myLocation.getLatitude();
                LatLng latLng = new LatLng(latitude, longitude);
                Circle circle = map.addCircle(new CircleOptions()
                        .center(latLng)
                        .radius(3000)
                        .strokeWidth(1)
                        .strokeColor(Color.WHITE)
                        .fillColor(Color.argb(50, 54, 161, 196))
                        .clickable(true));


                createMarker(2);
//                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
//                        Uri.parse("https://www.google.com/maps/dir/?api=1&origin=Space+Needle+Seattle+WA&destination=Pike+Place+Market+Seattle+WA&travelmode=bicycling"));
//                startActivity(intent);




                if (map != null)
                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 11));
            }
        });
    }

    private void createMarker(int kategori) {

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000, 10, locationListener);
        Location myLocation = locationManager.getLastKnownLocation( LocationManager.PASSIVE_PROVIDER );
        Location location = new Location( myLocation );
        Location target     = new Location("target");

        if(tokoList != null) {
            if (tokoList.getData() != null && !tokoList.getData().isEmpty()) {
                map.clear();

                for (int i = 0; i < tokoList.getData().size(); i++) {
                    TokoDataModel data  = tokoList.getData().get(i);
                    String storeType    = data.getDesc().toLowerCase();

                    double latitude     = Utils.convertStringToDouble(data.getLat());
                    double longitude    = Utils.convertStringToDouble(data.getLon());
                    LatLng vLocation    = new LatLng(latitude, longitude);

                    if(kategori == 1 && data.getKat() == 1) { //kategori == 1 && compareToko.contains(storeType)
                        target.setLatitude(latitude);
                        target.setLongitude(longitude);
                        if(location.distanceTo(target) < 5000) {
                            map.addMarker(new MarkerOptions()
                                    .position(vLocation).title(data.getNmToko()));
                        }
                    } else if(kategori == 2 && data.getKat() == 2) {
                        target.setLatitude(latitude);
                        target.setLongitude(longitude);

                        if(location.distanceTo(target) < 5000) {
                            map.addMarker(new MarkerOptions()
                                    .position(vLocation).title(data.getNmToko()));
                        }
                    }

//                    if(kategori == 1 && data.getKat() == 1) { //kategori == 1 && compareToko.contains(storeType)
//                        target.setLatitude(latitude);
//                        target.setLongitude(longitude);
//                         map.addMarker(new MarkerOptions().position(vLocation).title(data.getNmToko()));
//
//                    } else if(kategori == 2 && data.getKat() == 2) {
//                        target.setLatitude(latitude);
//                        target.setLongitude(longitude);
//                        map.addMarker(new MarkerOptions().position(vLocation).title(data.getNmToko()));
//
//                    }
                }
            }
        }
    }

    private void showToast(String text) {
        Toast.makeText(getActivity(), text, Toast.LENGTH_LONG).show();
    }

    private void loadJSON(){
        Rest.make(APIService.class).store().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String jsonString   = Utils.getResponse(response);
                    Gson gson           = new Gson();
                    tokoList            = gson.fromJson(jsonString, TokoListModel.class);

                    Log.i(Constant.TAG, "json123 -- " + jsonString);
                    if(tokoList != null) {
                        if (tokoList.getData() != null && !tokoList.getData().isEmpty()) {
                            adapter.setList(tokoList.getData());
                            adapter.notifyDataSetChanged();
                            recyclerView.setAdapter( adapter );
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("Error", t.getMessage());
            }
        });
    }

    @SuppressLint("MissingPermission")
    private void moveCamera() {
        locationManager.requestLocationUpdates( LocationManager.GPS_PROVIDER, 2000, 10, locationListener );
        Location myLocation = locationManager.getLastKnownLocation( LocationManager.PASSIVE_PROVIDER );
        double longitude = myLocation.getLongitude();
        double latitude = myLocation.getLatitude();

        LatLng latLng = new LatLng( latitude, longitude );


        if (map != null)
            map.moveCamera( CameraUpdateFactory.newLatLngZoom( latLng, 11 ) );


    }


    private final LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            longitude = location.getLongitude();
            latitude = location.getLatitude();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        if (ActivityCompat.checkSelfPermission( getActivity(), Manifest.permission.ACCESS_FINE_LOCATION )
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission( getActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        map.setMyLocationEnabled( true );
        initMaps();
        moveCamera();

    }


    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, int[] grantResults) {
        if (requestCode == MY_LOCATION_REQUEST_CODE) {
            if (permissions.length == 1 &&
                    permissions[0] == Manifest.permission.ACCESS_FINE_LOCATION &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                map.setMyLocationEnabled( true );
            } else {
                // Permission was denied. Display an error message.
            }
        }
    }

    @Override
    public void onItemClick(View view, int position) {
        openMaps(position);
    }
    @Override
    public void onLongItemClick(View view, int position) {

    }
}
