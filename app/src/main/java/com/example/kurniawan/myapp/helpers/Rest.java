package com.example.kurniawan.myapp.helpers;

import android.text.TextUtils;
import android.util.Log;

import com.example.kurniawan.myapp.helpers.converter.JSONConverterFactory;
import com.example.kurniawan.myapp.utils.Constant;
import com.example.kurniawan.myapp.utils.NetworkHelper;
import com.example.kurniawan.myapp.utils.Singleton;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Yudi Rahmat
 * Email yudirahmat7@gmail.com
 */

public class Rest {

    private static final String CACHE_CONTROL = "Cache-Control";

    private static Retrofit.Builder builder;
    private static OkHttpClient.Builder httpClient;
    private static Retrofit retrofit;

    public static Retrofit retrofit() {
        return retrofit;
    }

    static {
        httpClient = new OkHttpClient.Builder();

        httpClient.connectTimeout(60, TimeUnit.SECONDS);
        httpClient.readTimeout(60, TimeUnit.SECONDS);

        builder = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().registerTypeAdapter(Date.class, new LongToDate()).create()))
                .addConverterFactory(JSONConverterFactory.create());
    }

    public static <S> S make(Class<S> serviceClass) {
        return make(Constant.BASE_API, serviceClass, false);
    }

    public static <S> S make(Class<S> serviceClass, boolean cached) {
        return make(Constant.BASE_API, serviceClass, cached);
    }

    public static <S> S make(String baseUrl, Class<S> serviceClass) {
        return make(baseUrl, serviceClass, false);
    }

    public static <S> S make(String baseUrl, Class<S> serviceClass, boolean cached) {

        builder.baseUrl((baseUrl.endsWith("/")) ? baseUrl : baseUrl.concat("/"));

        String authToken = "none";
        if (Singleton.getInstance().getAuthToken() != null) {
            authToken = Singleton.getInstance().getAuthToken();
        }

        if (!TextUtils.isEmpty(authToken)) {
            AuthenticationInterceptor authInterceptor = new AuthenticationInterceptor(authToken);
            if (!httpClient.interceptors().contains(authInterceptor))
                httpClient.addInterceptor(authInterceptor);
        }

        if (cached) {
            Interceptor offlineCacheInterceptor = provideOfflineCacheInterceptor();
            if (!httpClient.interceptors().contains(offlineCacheInterceptor))
                httpClient.addInterceptor(offlineCacheInterceptor);

            Interceptor networkInterceptor = provideCacheInterceptor();
            if (!httpClient.networkInterceptors().contains(networkInterceptor))
                httpClient.addNetworkInterceptor(networkInterceptor);

            httpClient.cache( provideCache() );
        }

        builder.client(httpClient.build());
        retrofit = builder.build();

        return retrofit.create(serviceClass);
    }

    private static class AuthenticationInterceptor implements Interceptor {
        private String authToken;

        AuthenticationInterceptor(String token) {
            this.authToken = token;
        }

        public Response intercept(Chain chain) throws IOException {
            return chain.proceed(chain.request().newBuilder().header("Authorization", "Bearer " + this.authToken).build());
        }
    }

    public static Interceptor provideCacheInterceptor ()
    {
        return new Interceptor()
        {
            @Override
            public Response intercept (Chain chain) throws IOException
            {
                Response response = chain.proceed( chain.request() );

                // re-write response header to force use of cache
                CacheControl cacheControl = new CacheControl.Builder()
                        .maxAge(1, TimeUnit.MINUTES )
                        .build();

                return response.newBuilder()
                        .header( CACHE_CONTROL, cacheControl.toString() )
                        .build();
            }
        };
    }

    public static Interceptor provideOfflineCacheInterceptor ()
    {
        return new Interceptor()
        {
            @Override
            public Response intercept (Chain chain) throws IOException
            {
                Request request = chain.request();

                if ( !NetworkHelper.isConnectedToInternet(Singleton.getInstance().getApplicationContext()) )
                {
                    CacheControl cacheControl = new CacheControl.Builder()
                            .maxStale( 7, TimeUnit.DAYS )
                            .build();

                    request = request.newBuilder()
                            .cacheControl( cacheControl )
                            .build();
                }

                return chain.proceed( request );
            }
        };
    }

    private static Cache provideCache ()
    {
        Cache cache = null;
        try
        {
            cache = new Cache( new File( Singleton.getInstance().getCacheDir(), "http-cache" ),
                    10 * 1024 * 1024 ); // 10 MB
        }
        catch (Exception e)
        {
            Log.e("REZA", "Could not create Cache!" );
        }
        return cache;
    }

    private static class LongToDate implements JsonDeserializer<Date> {

        @Override
        public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            return new Date(json.getAsJsonPrimitive().getAsLong());
        }
    }
}
