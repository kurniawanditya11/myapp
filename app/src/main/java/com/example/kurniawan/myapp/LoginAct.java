package com.example.kurniawan.myapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.kurniawan.myapp.admin.AdminActivity;

public class LoginAct extends AppCompatActivity {
    EditText username, password;
    Button btnlogin;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate( savedInstanceState );
        setContentView(R.layout.login_layout);

        username = (EditText) findViewById( R.id.username );
        password = (EditText) findViewById( R.id.password );
        btnlogin = (Button) findViewById( R.id.btnlogin );

        btnlogin.setOnClickListener( new View.OnClickListener(){
            @Override
            public void onClick(View v){
                String usernameKey = username.getText().toString();
                String passwordKey = password.getText().toString();

                if (usernameKey.equals( "user" )&& passwordKey.equals( "user" )){
                    //jika login berhasil
                    Toast.makeText( getApplicationContext(),"Login Sukses", Toast.LENGTH_SHORT ).show();
                    Intent intent = new Intent( LoginAct.this, BottomActivity.class );
                    LoginAct.this.startActivity( intent );
                }
                else if  (usernameKey.equals( "admin" )&& passwordKey.equals( "admin123" )){
                    //jika login berhasil
                    Toast.makeText( getApplicationContext(),"Login Sukses", Toast.LENGTH_SHORT ).show();
                    Intent intent = new Intent( LoginAct.this, AdminActivity.class );
                    LoginAct.this.startActivity( intent );
                }else{
                    //jika gagal login
                    AlertDialog.Builder builder = new AlertDialog.Builder( LoginAct.this );
                    builder.setMessage( "Username atau password anda salah" )
                            .setNegativeButton( "Retry", null ).create().show();
                }
            }
        } );

    }

}
