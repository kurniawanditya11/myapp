package com.example.kurniawan.myapp.admin;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.kurniawan.myapp.R;
import com.example.kurniawan.myapp.helpers.Rest;
import com.example.kurniawan.myapp.helpers.services.APIService;
import com.example.kurniawan.myapp.utils.NetworkHelper;
import com.example.kurniawan.myapp.utils.Singleton;
import com.example.kurniawan.myapp.utils.Utils;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class Tambahtoko extends AppCompatActivity {


    EditText nm_toko, lattitude, longitude, deskripsi, alamat;
    Button btnAddToko;
    Intent intent;

    int success;
    ConnectivityManager conMgr;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_tambahtoko );
        conMgr = (ConnectivityManager) getSystemService( Context.CONNECTIVITY_SERVICE );
        {
            if (conMgr.getActiveNetworkInfo() != null
                    && conMgr.getActiveNetworkInfo().isAvailable()
                    && conMgr.getActiveNetworkInfo().isConnected()) {
            } else {
                Toast.makeText( getApplicationContext(), "No Internet Connection",
                        Toast.LENGTH_LONG ).show();
            }
        }
        nm_toko = (EditText) findViewById( R.id.tv_nm_toko );
        lattitude = (EditText) findViewById( R.id.tv_toko_lat );
        longitude = (EditText) findViewById( R.id.tv_toko_lon );
        deskripsi = (EditText) findViewById( R.id.tv_toko_desk );
        alamat = (EditText) findViewById( R.id.tv_toko_alamat );

        btnAddToko = (Button) findViewById( R.id.btnAddToko );
        btnAddToko.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                String namatoko = nm_toko.getText().toString();
                String lat = lattitude.getText().toString();
                String lon = longitude.getText().toString();
                String desk = deskripsi.getText().toString();
                String alamt = alamat.getText().toString();

//                String namatoko = "test";
//                String lat = "test";
//                String lon = "test";
//                String desk = "test";
//                String alamt = "test";


//                if(nm_toko != null && lattitude != null && longitude != null && deskripsi != null && alamat != null) {
//                    Toast.makeText( getApplicationContext(),"data ada", Toast.LENGTH_SHORT ).show();
//                }
//                else{
//                    Toast.makeText( getApplicationContext(),"data tidak ada", Toast.LENGTH_SHORT ).show();
//                }

                if (conMgr.getActiveNetworkInfo() != null
                        && conMgr.getActiveNetworkInfo().isAvailable()
                        && conMgr.getActiveNetworkInfo().isConnected()) {
                    addToko( namatoko, lat, lon, desk, alamt );
                } else {
                    Toast.makeText( getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT ).show();
                }
            }
        } );
    }
    private void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
    }

    private void addToko(final String nm_toko,final String lattitude,final String longitude,final String deskripsi,final String alamat){
        if (NetworkHelper.isConnectedToInternet( Singleton.getInstance().getApplicationContext())) {
            Rest.make(APIService.class).addstore(nm_toko,lattitude,longitude, deskripsi,alamat)
                    .enqueue( new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
//                    hideDialog();
                    try {
                        if(response != null && response.isSuccessful()) {
                            String jsonString = Utils.getResponse(response);
                            JSONObject jsonObj = new JSONObject(jsonString);
                            int status = jsonObj.optInt( "success" );
                            Toast.makeText(Tambahtoko.this," pesan :  "+ jsonObj.optString( "message" ),Toast.LENGTH_LONG).show();
//
                            if(status == 1) {
                                onBackPressed();
                            }
                        } else {
                            showToast(response.code() + "\n\n" + "Data gagal ditambahkan");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        showToast(0 + "\n\n" + "Data gagal ditambahkan");
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    showToast(500 + "\n\n" + "Data gagal ditambahkan");
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        intent = new Intent( Tambahtoko.this, TokoActivity.class );
        startActivity( intent );
        finish();
    }
}
