package interfaces;

import android.view.View;

/**
 * Created by Yudi Rahmat
 * Email yudirahmat7@gmail.com
 */

public interface IAdapterView {
    void onItemClick(View view, int position);
    void onLongItemClick(View view, int position);
}
